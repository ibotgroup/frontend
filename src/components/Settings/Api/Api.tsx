import styled from "styled-components";
import { FC } from "react";

const BotsComponent = styled.div`
  position: relative;
`;

const Title = styled.div``;

const BotsTable = styled.table`
  width: 100%;
  border: none;
  border-collapse: collapse;

  caption-side: bottom;
  td,
  th {
    border: none;
  }

  td {
    padding: 5px 10px;
  }

  tbody tr {
    :hover {
      background-color: blueviolet;
    }
  }
  thead > tr {
    background: lightblue;
  }
  caption {
    font-size: 0.9em;
    padding: 5px;
    font-weight: bold;
  }
`;

export const Api: FC = () => {
  return (
    <BotsComponent>
      <Title>Binance api list</Title>
      <BotsTable>
        <colgroup>
          <col />
          <col />
          <col />
        </colgroup>
        <thead>
          <tr>
            <th>Type</th>
            <th>Key</th>
            <th>Secret</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Binance</td>
            <td>key</td>
            <td>secret</td>
          </tr>
        </tbody>
      </BotsTable>
    </BotsComponent>
  );
};
