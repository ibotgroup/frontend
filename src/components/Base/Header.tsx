import styled from "styled-components";
import { FC } from "react";

const HeaderSection = styled.section`
  text-align: left;
  font-size: 50px;
  border: 1px solid purple;
  height: 100px;
`;

export const Header: FC = () => {
  return <HeaderSection>header</HeaderSection>;
};
