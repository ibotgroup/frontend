import React, { useEffect } from 'react';
import { createChart } from 'lightweight-charts';

export function Chart() {
    const ref = React.useRef();

    useEffect(() => {
        const chart = createChart(ref.current, { width: 1000, height: 600 });
        //upper grid limit
        const gridUpperLimit = chart.addLineSeries({
            lineWidth: 4,
            lineStyle: 2,
            lineType: 1,
            color: 'red',
        });
        gridUpperLimit.setData([
            { time: '2021-06-08', value: 2800 },
        ]);

        const line2 = chart.addLineSeries({
            lineWidth: 1,
            lineStyle: 2,
            lineType: 1,
        });
        line2.setData([
            { time: '2021-06-08', value: 2798.42 },
        ]);

        const line3 = chart.addLineSeries({
            lineWidth: 1,
            lineStyle: 2,
            lineType: 1,
        });
        line3.setData([
            { time: '2021-06-08', value: 2796.84 },
        ]);

        const line4 = chart.addLineSeries({
            lineWidth: 1,
            lineStyle: 2,
            lineType: 1,
        });
        line4.setData([
            { time: '2021-06-08', value: 2795.26 },
        ]);


        //lower grid limit
        const gridLowerLimit = chart.addLineSeries({
            lineWidth: 4,
            lineStyle: 2,
            lineType: 1,
            color: 'red',
        });
        gridLowerLimit.setData([
            { time: '2021-06-08', value: 2770 },
        ]);
    }, []);

    return (
        <>
            <div ref={ref} />
        </>
    );
}