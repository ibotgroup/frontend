import { Header } from "./Base/Header";
import { Sidebar } from "./Base/Sidebar";
import { Main } from "./Base/Main";
import styled from "styled-components";
import { FC } from "react";

const AppComponent = styled.div``;
const ContentSection = styled.div`
  overflow: hidden;
`;

export const App: FC = () => {
  return (
    <>
      <AppComponent>
        <Header />

        <ContentSection>
          <Sidebar />

          <Main />
        </ContentSection>
      </AppComponent>
    </>
  );
};
