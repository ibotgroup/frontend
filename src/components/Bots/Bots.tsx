import styled from "styled-components";
import { FC } from "react";

const BotsComponent = styled.div`
  position: relative;
`;

const Title = styled.div``;

const BotsTable = styled.table`
  width: 100%;
  border: none;
  border-collapse: collapse;

  caption-side: bottom;
  td,
  th {
    border: none;
  }

  td {
    padding: 5px 10px;
  }

  tbody tr {
    :hover {
      background-color: blueviolet;
    }
  }
  thead > tr {
    background: lightblue;
  }
  caption {
    font-size: 0.9em;
    padding: 5px;
    font-weight: bold;
  }
`;

export const Bots: FC = () => {
  return (
    <BotsComponent>
      <Title>Bot list</Title>
      <BotsTable>
        <colgroup>
          <col />
          <col />
          <col />
        </colgroup>
        <thead>
          <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
          </tr>
          <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
          </tr>
          <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
          </tr>
        </tbody>
      </BotsTable>
    </BotsComponent>
  );
};
