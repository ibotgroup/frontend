import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { FC } from "react";

const SidebarSection = styled.section`
  border: 1px solid deeppink;
  float: left;
  width: 300px;
  margin-bottom: -5000px;
  padding-bottom: 5000px;
`;

const Menu = styled.div`
  font-size: 20px;
  border: 1px solid yellow;
  color: blue;
`;

const NavigationElement = styled.div``;

const Navigation = styled(NavLink)`
  text-decoration: none;

  &:visited {
    color: blue;
    text-decoration: none;
  }

  &:hover {
    color: blue;
    text-decoration: underline;
  }
`;

const activeNavigation = {
  color: "blue",
  textDecoration: "underline",
};

export const Sidebar: FC = () => {
  return (
    <SidebarSection>
      <Menu>
        <NavigationElement>
          <Navigation exact={true} to="/" activeStyle={activeNavigation}>
            Information
          </Navigation>
        </NavigationElement>
        <NavigationElement>
          <Navigation exact={true} to="/bots" activeStyle={activeNavigation}>
            Bots
          </Navigation>
        </NavigationElement>
        <NavigationElement>
          <Navigation
            exact={true}
            to="/settings/api"
            activeStyle={activeNavigation}
          >
            Api settings
          </Navigation>
        </NavigationElement>
      </Menu>
    </SidebarSection>
  );
};
