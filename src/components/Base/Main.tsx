import { Switch, Route } from "react-router-dom";
import styled from "styled-components";
import { FC } from "react";
import { Main as MainComponent } from "../Main/Main";
import { Bots } from "../Bots/Bots";
import { Api } from "../Settings/Api/Api";

const MainSection = styled.section`
  width: calc(100% - 300px) !important;
  max-width: calc(100% - 300px) !important;
  position: absolute;
  left: 300px;
`;

export const Main: FC = () => {
  return (
    <MainSection>
      <Switch>
        <Route exact={true} path="/">
          <MainComponent />
        </Route>
        <Route exact={true} path="/bots">
          <Bots />
        </Route>
        <Route exact={true} path="/settings/api">
          <Api />
        </Route>
      </Switch>
    </MainSection>
  );
};
